#!/usr/local/bin/python
# -*- coding: utf-8 -*-


from __future__ import print_function
from __future__ import unicode_literals

import sys
import os
import string
import random

# Parooli genereerimise funktsioon
# http://stackoverflow.com/questions/7479442/high-quality-simple-random-password-generator
def generate_password():
  password_chars = string.ascii_uppercase + string.ascii_letters + string.digits + "_-"
  password = ''.join(random.choice(password_chars) for i in range(20))
  return password


# Parameetrite kontroll
if len(sys.argv) != 3:
  print("Kaivita programm:", sys.argv[0], "sisendafailinimi", "valjundfailinimi")
  sys.exit(1)

# Omistame vaartused sisendfailile ja valjundfailile 
input_file = sys.argv[1]
output_file = sys.argv[2]

# Sisendfaili kontroll
try:
  read_file = open(input_file)  
except IOError:
  print("Sisendfaili %s ei saa lugeda" % input_file)
  sys.exit(2)
except Exception:
  print("Sisendfaili %s lugemisel tekkis ootamatu torge" % input_file)
  sys.exit(3)

#Valjundfaili kontroll
try:
  write_file = open(output_file, 'w')
except IOError:
  print("Valjundfaili %s ei saa kirjutada" % output_file)
  sys.exit(4)
except Exception:
  print("Valjundfaili %s kirjutamisel tekkis ootamatu viga" % output_file)
  sys.exit(5)

# Loeme sisendfailist ridu
for read_line in read_file:
  #Kontrollime kas rida algab numbriga
  if (read_line[0].isdigit()):

    # Eemaldame numbri ja muud andmed
    line = read_line.split('\t')
    user = line[1:]
    
    # Kasutaja andmed massiivi
    data = str(user[0]).split()

    eesnimi = data[0]
    perenimi = data[1]
    username = (eesnimi[:1]+perenimi).lower()[:8]
    email = eesnimi.lower()+"."+perenimi.lower()+"@itcollege.ee"
    password = generate_password()
  
    write_file.write(username+","+eesnimi+" "+perenimi+","+email+","+password+os.linesep)

read_file.close()
write_file.close()