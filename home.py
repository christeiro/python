#!/usr/local/bin/python
# -*- coding: utf-8 -*-
# Skript loeb esimese parameetrina antud failist sisse URL-id ja nende järel olevad stringid. 
# String ja URL eraldatakse, seejärel tehakse päring vastava URLi pihta. 
# Vastuse lähtekoodist otsitakse URLi järel olnud stringi. 
# Teise parameetrina antud faili kirjutatakse uuesti URL, otsitav string ja "JAH/EI" vastavalt sellele, kas otsitav string leiti või mitte.


from __future__ import print_function
from __future__ import unicode_literals

import sys
import os
import urllib2


# Parameetrite kontroll
if len(sys.argv) != 3:
  print("Kaivita programm:", sys.argv[0], "sisendafailinimi", "valjundfailinimi")
  sys.exit(1)

# Sisendfaili kontroll
try:
  read_file = open(sys.argv[1])  
except IOError:
  print("Faili ei saa lugeda")
  sys.exit(2)
except Exception:
  print("Faili lugemisel tekkis ootamatu torge")
  sys.exit(3)

#Valjundfaili kontroll
try:
  write_file = open(sys.argv[2], 'w')
except IOError:
  print("Faili ei saa kirjutada")
  sys.exit(4)
except Exception:
  print("Faili kirjutamisel tekkis ootamatu viga")
  sys.exit(5)

# Loeme sisendfailist ridu
for line in read_file:
  rida = line.split()
  url = rida[0]
  otsi_sonad = rida[1:]

  is_url_valid = 1

  # Proovime avada URL'i - sisendfaili esimene vaartus. 
  try:
    open_url = urllib2.urlopen(url)
  except Exception:
    print("URL: %s avamine ebaonnestus." % url)
    is_url_valid = 0

  if (is_url_valid == 1):
    url_content = open_url.read()
    # Valjundfaili ridade loomine
    result_line = []
    result_line.append(url)

    for otsi in otsi_sonad:
      tulemus = url_content.find(otsi)
      if tulemus < 0:
        result_line.append("%s:EI" % otsi)
      else:
        result_line.append("%s:JAH" % otsi)
    write_file.write(' '.join(result_line)+os.linesep)

read_file.close()
write_file.close()